---
title: "Satellite watching time"
date: 2020-05-14
tags:
  - "2020"
---
[Trying to catch a glimpse of the ISS, the Starlink satellite constellation and other satellites, but you never know when they're passing 'close by'?](https://james.darpinian.com/satellites/)

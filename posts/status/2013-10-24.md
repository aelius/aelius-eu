---
title: "2013-10-24"
date: 2013-10-24
tags:
  - "2013"
---
Deep within the "Social" side of the Internet tonight. On one side discussing with strangers "The Hobbit" movie and on the other side discussing the nature of photons.
---
title: "2012-04-01"
date: 2012-04-01
tags:
  - "2012"
---
Dia das mentiras é como acender a luz quando não há electricidade, por mais que saibamos continuamos a cair no mesmo erro.
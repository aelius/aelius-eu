---
title: "2017-10-11"
date: 2017-10-11
tags:
  - "2017"
---
Different understanding of Customer Service in the shop:<br/><br/>Unity Media:<br/>"Hallo. Sprechen Sie Englisch?"<br/>UM: "Nein! Wir sind in Deutschland." (serious face)<br/><br/>Vodafone:<br/>"Hallo. Sprechen Sie Englisch?"<br/>VF: "Nur ein bisschen." (with a kind smile)<br/>And we managed to communicate just fine
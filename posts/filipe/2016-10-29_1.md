---
title: "Shelved"
date: 2016-10-29
tags:
  - "2016"
---
Why spend 150€ on a bed, when a 35€ shelving unit is just as good?
And includes storage.

![bed](/img/2016-10-29.jpg)
